Documentos relacionados al proyecto de grado de Tatiana y Brian.

Formato del libro normas apa 7 versión

Título de Proyecto: Desarrollo aplicativo móvil y web como soporte al proyecto “Aire + Limpio = 10R” convenio UDES- UniRed-Ecopetrol.

OBJETIVO GENERAL.

Implementar un prototipo multiplataforma empleando el framework de Ionic para el desarrollo de aplicaciones híbridas, que permita la interacción de usuarios en la prueba piloto del proyecto "Aire + Limpio = 10R".

OBJETIVOS ESPECÍFICOS.

• Identificar los requisitos necesarios para el desarrollo de la aplicación multiplataforma, cumpliendo con las especificaciones del proyecto “Aire + Limpio = 10R”.

• Diseñar la herramienta software de tal forma que permita la interacción de los usuarios, teniendo en cuenta los requisitos identificados.

• Desarrollar la aplicación multiplataforma que facilite las funcionalidades específicas del sistema, empleando el framework de Ionic para la elaboración de herramientas híbridas en dispositivos iOS, Android y entorno Web.

• Validar la herramienta software desarrollada con los integrantes del equipo, permitiendo la interacción de usuarios en la prueba piloto del proyecto “Aire + Limpio = 10R”.